﻿using System.Collections.Generic;

namespace ExerciseLibraryService
{
    public class ExerciseLibraryService : IExerciseLibraryService
    {
        public List<Exercise> GetExercises()
        {
            return new List<Exercise> {
                new Exercise() { ExerciseId = 1, ExerciseName = "Power Clean"},
                new Exercise() { ExerciseId = 2, ExerciseName = "Hang Power Clean"},
                new Exercise() { ExerciseId = 3, ExerciseName = "Clean And Jerk"},
                new Exercise() { ExerciseId = 4, ExerciseName = "Snatch"},
                new Exercise() { ExerciseId = 5, ExerciseName = "Power Snatch" }
             };            
        }
    }
}
