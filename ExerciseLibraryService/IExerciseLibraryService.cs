﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ExerciseLibraryService
{
    [ServiceContract]
    public interface IExerciseLibraryService
    {
        [WebGet(UriTemplate = "/GetExercisesData", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<Exercise> GetExercises();        
    }


    [DataContract]
    public class Exercise
    {
        int id;
        string exerciseName;

        [DataMember]
        public int ExerciseId { get { return id; } set { id = value; } }

        [DataMember]
        public string ExerciseName { get { return exerciseName; } set { exerciseName = value; } } 
    }
}
